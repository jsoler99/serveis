<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmaciaGuardia extends Model
{
    
	protected $table = 'farmacies_guardies';
	protected $guarded = ['id'];

	public function farmacia() 
	{
		return $this->belongsTo(Farmacia::class);
	}
}
