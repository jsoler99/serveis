<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poblacio extends Model
{
    protected $table = 'poblacions';

    protected $guarded = ['id'];

    public function farmacies()
    {
    	return $this->hasMany(Farmacia::class);
    }
}
