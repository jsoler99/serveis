<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class controladorprova extends Controller
{
    public function index() {
		return "Estem a l'index";
	}
	public function create() {
        return "Estem creant";
	}
	public function store() {
        return "Estem guardant ";
	}
	public function show() {
        return "Estem mostrant ";
	}
	public function edit($id) {
        return "Estem editant".$id;
	}
	public function update($id) {
        return "Estem actualitzant".$id;
	}
	public function destroy($id) {
        return "Estem esborrant".$id;
	}
	public function esborra($id) {
        return "Estem esborrant2 ".$id;
	}
}
