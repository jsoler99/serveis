<?php

namespace App\Http\Controllers;

use App\Poblacio;
use Illuminate\Http\Request;

class PoblacionsController extends Controller
{
    
    public function index() 
    {
    	$poblacions = Poblacio::all();

    	return view('poblacions.index', compact('poblacions'));
    }

    public function create()
    {
    	return view('poblacions.create');
    }

    public function store(Request $request) {
  		$poblacio = new Poblacio;
  		$poblacio->fill($request->all())->save();

    	return redirect('/poblacions');
    }

}
