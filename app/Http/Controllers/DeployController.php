<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

class DeployController extends Controller
{
    public function index() 
    {
    	return view('deploy.index');
    }

	public function deploy($live = true)
    {
        $result = [];

        $process = new Process('/home/bitnami/.composer/vendor/bin/envoy run deploy');
        $process->setTimeout(3600);
        $process->setIdleTimeout(300);
        $process->setWorkingDirectory(base_path());
        $process->run(
            function ($type, $buffer) use ($live, &$result) {
                $buffer = str_replace('[127.0.0.1]: ', '', $buffer);

                if ($live) {
                    echo $buffer . '</br />';
                }

                $result[] = $buffer;
            }
        );

        return $result;
    }
}
