<?php

namespace App\Http\Controllers;

use App\Farmacia;
use App\Poblacio;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FarmaciaController extends Controller
{
    
    public function index() {
    	$farmacies = Farmacia::all();
        $poblacions = Poblacio::has('farmacies')->get();

    	return view('farmacies.index', compact('farmacies', 'poblacions'));
    }

    public function create() {
    	$poblacions = Poblacio::all();
        $farmacia = new Farmacia;

    	return view('farmacies.create', compact('poblacions', 'farmacia'));
    }

  	public function store(\App\Http\Requests\Farmacies\CreateFarmaciesForm $form) {
        $farmacia = $form->persist();

        return redirect('/farmacies');
    }

    public function edit($id) {
        $poblacions = Poblacio::all();
        $farmacia = Farmacia::find($id);

        return view('farmacies.edit', compact('poblacions', 'farmacia'));
    }

    public function update(\App\Http\Requests\Farmacies\EditFarmaciesForm $form) {
        $farmacia = $form->persist();

        return redirect('/farmacies');
    }

    public function gestionarCalendari($poblacio) {
        $poblacio = Poblacio::with('farmacies')->find($poblacio);
        $guardies = \App\FarmaciaGuardia::wherePoblacioId($poblacio->id)->where('data','>=', Carbon::now()->startOfMonth())->where('data','<=', Carbon::now()->endOfMonth())->latest()->get();
        
        return view('farmacies.calendari', compact('poblacio', 'guardies'));
    }

    public function actualitzaCalendari(\App\Http\Requests\Farmacies\CalendariGuardiesForm $form, $poblacio) {
        $form->persist();

        return 'ok!';
    }

}
