<?php

namespace App\Http\Requests\Farmacies;

use App\Farmacia;
use Illuminate\Foundation\Http\FormRequest;

class EditFarmaciesForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required',
            'poblacio_id' => 'required',
        ];
    }

    public function persist()
    {
        $farmacia = Farmacia::find($this->route('id'));
        $farmacia->fill($this->all())->save();

        return $farmacia;
    }
}
