<?php

namespace App\Http\Requests\Farmacies;

use App\Farmacia;
use Illuminate\Foundation\Http\FormRequest;

class CreateFarmaciesForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required',
            'poblacio_id' => 'required',
        ];
    }

    public function persist()
    {
        $farmacia = new Farmacia;
        $farmacia->fill($this->all())->save();

        return $farmacia;
    }
}
