<?php

namespace App\Http\Requests\Farmacies;

use App\Farmacia;
use Illuminate\Foundation\Http\FormRequest;

class CalendariGuardiesForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function persist() 
    {
        if ($this->action == 'create')
        {
            $farmacia = Farmacia::find($this->farmacia);
            $farmacia->guardies()->create([
                'poblacio_id' => $farmacia->poblacio_id,
                'data' => $this->data
            ]);
        }
        else if ($this->action == 'delete')
        {
            \App\FarmaciaGuardia::where('data', $this->data)->where('farmacia_id', $this->farmacia)->delete();
        }

    }
}
