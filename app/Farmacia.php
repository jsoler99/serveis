<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmacia extends Model
{
    
    protected $guarded = ['id'];
    protected $table = 'farmacies';

    /**
     *	Relacions
     **/
    public function poblacio() 
    {
    	return $this->belongsTo(Poblacio::class);
    }

    public function guardies()
    {
    	return $this->hasMany(FarmaciaGuardia::class);
    }

}
