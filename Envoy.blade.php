@servers(['web' => '127.0.0.1'])

@task('foo', ['on' => 'web'])
	cd /Applications/MAMP/htdocs/serveis
    git pull origin master
@endtask

@task('deploy', ['on' => 'web'])
	cd /Applications/MAMP/htdocs/{{ $site }}
	git pull origin master
@endtask