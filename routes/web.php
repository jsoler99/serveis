<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('prova', function () {
    return "Hola des de prova";
});
Auth::routes();

Route::get('/poblacions', 'PoblacionsController@index');
Route::get('/poblacions/create', 'PoblacionsController@create');
Route::post('/poblacions/store', 'PoblacionsController@store');

Route::get('/home', 'HomeController@index');
Route::get('/farmacies', 'FarmaciaController@index');
Route::get('/farmacies/create', 'FarmaciaController@create');
Route::post('/farmacies/store', 'FarmaciaController@store');
Route::get('/farmacies/{id}/edit', 'FarmaciaController@edit');
Route::post('/farmacies/{id}/update', 'FarmaciaController@update');
Route::get('/farmacies/{poblacio}/calendari', 'FarmaciaController@gestionarCalendari');
Route::post('/farmacies/{poblacio}/actualitza-calendari', 'FarmaciaController@actualitzaCalendari');

Route::get('/deploy', 'DeployController@index');
Route::get('/deploy/do-it', 'DeployController@deploy');

