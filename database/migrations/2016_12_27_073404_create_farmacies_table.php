<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmacies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->integer('poblacio_id');
            $table->string('telefon');
            $table->string('adreca');
            $table->text('comentaris');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmacies');
    }
}
