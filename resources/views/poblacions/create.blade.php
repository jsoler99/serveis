@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Panell de control - Poblacions
                	<a class="pull-right" href="/poblacions">< Tornar</a>
                </div>

                <div class="panel-body">
            		<h4 style="overflow: hidden"> Crear nova població</h4>
	                <form method="post" action="/poblacions/store">
	                	{{ csrf_field() }}
		                <div class="row">
		                	<div class="col-md-6">
								<div class="form-group">
									<label for="nom">Nom</label>
								    <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="codi_postal">Codi postal</label>
								    <input type="text" name="codi_postal" class="form-control" id="codi_postal" placeholder="Codi Postal">
								</div>
							</div>
						</div>
  						<button type="submit" class="btn btn-default">Crear nova població</button>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
