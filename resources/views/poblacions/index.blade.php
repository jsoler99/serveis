@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Panell de control - Poblacions
                	<a class="pull-right" href="/home">< Tornar</a>
                </div>

                <div class="panel-body">
                	<div class="col-md-12">
                		<h4 style="overflow: hidden">Poblacions <a href="/poblacions/create" class="btn btn-primary btn-sm pull-right">Nova</a></h4>
	                    <table class="table">
	                    	<tr>
								<th>Nom</th>
								<th>Codi Postal</th>
								<th></th>
							</tr>
							@foreach($poblacions as $poblacio)
							<tr>
								<td>{{ $poblacio->nom }}</td>
								<td>{{ $poblacio->codi_postal }}</td>
								<td class="text-right"><a href="/poblacions/{{ $poblacio->id }}/edit">modificar</a></td>
							</tr>
							@endforeach
	                    </table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
