@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Panell de control</div>

                <div class="panel-body">
                    <div class="col-md-4">
                        <a href="/farmacies">
                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Farmàcies</b></div>
                                <div class="panel-body">
                                    Panel content
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="/deploy">
                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Deploy</b></div>
                                <div class="panel-body">
                                    Panel content
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
