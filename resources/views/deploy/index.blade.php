@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Panell de control - Deploys
                	<a class="pull-right" href="/home">< Tornar</a>
                </div>

                <div class="panel-body">
                	<div class="col-md-12">
	                    <table class="table">
	                    	<tr>
								<th>Nom</th>
								<th></th>
								<th></th>
							</tr>
							<tr>
								<td>El 9 Nou</td>
								<td><button class="btn btn-primary">Deploy</button>
							</tr>
							
	                    </table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
