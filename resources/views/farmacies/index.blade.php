@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Panell de control - Farmàcies
                	<a class="pull-right" href="/home">< Tornar</a>
                </div>

                <div class="panel-body">
                	<div class="col-md-6">
                		<h4 style="overflow: hidden">Farmàcies <a href="/farmacies/create" class="btn btn-primary btn-sm pull-right">Nova</a></h4>
	                    <table class="table">
	                    	<tr>
								<th>Nom</th>
								<th>Població</th>
								<th></th>
							</tr>
							@foreach($farmacies as $farmacia)
							<tr>
								<td>{{ $farmacia->nom }}</td>
								<td>{{ $farmacia->poblacio->nom }}</td>
								<td class="text-right"><a href="/farmacies/{{ $farmacia->id }}/edit">modificar</a></td>
							</tr>
							@endforeach
	                    </table>
					</div>
					<div class="col-md-6">
						<h4 style="overflow: hidden">Poblacions <a href="/poblacions/create" class="btn btn-primary btn-sm pull-right">Nova</a></h4>
						<table class="table">
	                    	<tr>
								<th>Nom</th>
								<th class="text-center">Num. Farmàcies</th>
								<th></th>
							</tr>
							@foreach($poblacions as $poblacio)
							<tr>
								<td>{{ $poblacio->nom }}</td>
								<td class="text-center">{{ $poblacio->farmacies->count() }}</td>
								<td class="text-right"><a href="/farmacies/{{ $poblacio->id }}/calendari">gestionar calendari</a></td>
							</tr>
							@endforeach
	                    </table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
