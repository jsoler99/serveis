{{ csrf_field() }}
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="nom">Nom de la farmàcia</label>
		    <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom de la farmàcia" value="{{ old('nom', $farmacia->nom) }}">
		</div>
		<div class="form-group">
			<label for="telefon">Telèfon</label>
		    <input type="text" name="telefon" class="form-control" id="telefon" placeholder="Telèfon" value="{{ old('telefon', $farmacia->telefon) }}">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
			<label for="poblacio">Població</label>
			<select id="poblacio" class="form-control" name="poblacio_id">
				@foreach($poblacions as $poblacio)
					<option value="{{ $poblacio->id }}">{{ $poblacio->nom }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="adreca">Adreça</label>
		    <input type="text" name="adreca" class="form-control" id="adreca" placeholder="Adreça" value="{{ old('adreca', $farmacia->adreca) }}">
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label for="comentari">Comentaris</label>
		    <textarea class="form-control" name="comentaris" id="comentari" placeholder="Comentaris">{{ old('comentaris', $farmacia->comentaris) }}</textarea>
		</div>
	</div>
</div>