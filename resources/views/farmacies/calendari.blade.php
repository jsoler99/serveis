@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" id="calendari-farmacies">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Panell de control - Farmàcies
                	<a class="pull-right" href="/farmacies">< Tornar</a>
                </div>

                <div class="panel-body">
                    <div class="col-md-3">
                        <ul class="list-group">
                            @foreach($poblacio->farmacies as $farmacia)
                                <li class="list-group-item" @click="setFarmacia({{ $farmacia->id }}, '{{ $farmacia->nom }}')">{{ $farmacia->nom }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-9">
	                   <form method="post" action="/farmacies/store">
  						    <button type="submit" class="btn btn-default pull-right">Guardar canvis</button>
  
                            <?php $date = Carbon\Carbon::now()->startOfMonth() ?>
                            <?php $endOfMonth = Carbon\Carbon::now()->endOfMonth() ?>
                            <table class="table" cellpadding="0" cellspacing="0" class="SimpleCalendar">
                                <thead>
                                    <tr>
                                        <th class="text-center">Dilluns</th> 
                                        <th class="text-center">Dimarts</th> 
                                        <th class="text-center">Dimecres</th> 
                                        <th class="text-center">Dijous</th> 
                                        <th class="text-center">Divendres</th> 
                                        <th class="text-center">Dissabte</th> 
                                        <th class="text-center">Diumenge</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @for ($j = 0; $j < $date->dayOfWeek - 1; $j++ )<td></td>@endfor
                                        @for ($i = 0; $i < $endOfMonth->format('d'); ++$i )
                                            @if (($i + $j) % 7 == 0)</tr><tr>@endif
                                            <td id="{{ $i }}">
                                                <div class="day">
                                                    <div @click="fillFarmacia({{ $i }}, '2016-12-{{ $i + 1 }}')">{{ $i + 1 }} </div>
                                                    <?php $dia = $guardies->filter(function($item) use ($i) {
                                                        $number = $i < 10 ? '0' . ($i + 1) : ($i + 1);
                                                        return $item->data == '2016-12-' . $number;
                                                    }) ?>
                                                    @if (count($dia))
                                                        <div class="farmacia" style="position: relative;">
                                                            @foreach($dia as $dia)
                                                                <span data-farmacia="{{ $dia->farmacia_id }}">{{ $dia->farmacia->nom }}</span>
                                                                <i class="esborra-guardia fa fa-times" aria-hidden="true" @click="deleteGuardia({{ $i }}, '2016-12-{{ $i + 1 }}')"></i>
                                                            @endforeach
                                                        </div> 
                                                    @else
                                                    <div class="farmacia" style="display: none;position: relative;">
                                                        <span data-farmacia=""></span>
                                                        <i class="esborra-guardia fa fa-times" aria-hidden="true" @click="deleteGuardia({{ $i }}, '2016-12-{{ $i + 1 }}')"></i>
                                                    </div>
                                                    @endif
                                                </div>
                                            </td>
                                        @endfor
                                    </tr>
                                </tbody>
                            </table>
					   </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        var farmacies = new Vue({
            el: '#calendari-farmacies',
            data: {
                'farmacia' : {'nom' : 'Empty', 'id' : 'empty'},
                'guardies': [],
            },
            methods: {
                setFarmacia: function(id, nom) {
                    this.farmacia.id = id;
                    this.farmacia.nom = nom;
                },
                fillFarmacia: function(id, data) {
                    $('#' + id).find('span').html(this.farmacia.nom);
                    $('#' + id).find('span').attr('data-farmacia', this.farmacia.id);
                    $('#' + id + ' .farmacia').show();
                    $.post('/farmacies/{{ $poblacio->id }}/actualitza-calendari', {
                        '_token': $('meta[name=csrf-token]').attr('content'),
                        farmacia: this.farmacia.id,
                        data: data,
                        action: 'create'
                    });
                },
                deleteGuardia: function(id, data) {
                    $('#' + id + ' span').html('');
                    $('#' + id + ' .farmacia').hide();
                    $.post('/farmacies/{{ $poblacio->id }}/actualitza-calendari', {
                        '_token': $('meta[name=csrf-token]').attr('content'),
                        farmacia: $('#' + id).find('span').attr('data-farmacia'),
                        data: data,
                        action: 'delete'
                    });
                }
            }

        });
    </script>
@endsection
