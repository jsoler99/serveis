@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Panell de control - Farmàcies
                	<a class="pull-right" href="/farmacies">< Tornar</a>
                </div>

                <div class="panel-body">
            		<h4 style="overflow: hidden">Modificar farmàcia</h4>
            		@include('forms.errors')
	                <form method="post" action="/farmacies/{{ $farmacia->id }}/update">
		                @include('farmacies.form')
  						<button type="submit" class="btn btn-default">Guardar canvis</button>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
